import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { Observable } from 'rxjs'; 

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  items: Observable<any[]>;
  constructor (db: AngularFirestore, public afAuth: AngularFireAuth) {
    this.items = db.collection('hot_drinks').valueChanges();
    this.afAuth.authState.subscribe((uid) => {
      console.log(`${uid.displayName} is logged in`)
    })
  }
}
