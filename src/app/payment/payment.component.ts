import { Component, OnInit } from '@angular/core';
import { HotDrinkCart } from '../hot-drink/hot-drink.component';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { StripeUser } from '../auth-dialog/auth-dialog.component'

declare let Stripe: any

export interface ICheckoutLineItem {
  /**
   * Price of one item
   */
  amount: number;

  /**
   * ISO currency
   */
  currency: string;

  /**
   * Name of the item
   */
  name: string;

  /**
   * Amount of item
   */
  quantity: number;

  /** 
   * Description of the item 
   */
  description?: string;

  /**
   * URL to images of item
   */
  images?: string[];
}

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {

  stripeCart: ICheckoutLineItem[] = Array<ICheckoutLineItem>()
  stripe = new Stripe('pk_test_gdSHcrS8pKC6gB7wzuvCGoSF')

  sessionId: string
  cusId: string

  constructor(public afAuth: AngularFireAuth, public db: AngularFirestore) {
    this.afAuth.authState.subscribe((auth) => {
      let userDoc: AngularFirestoreDocument<StripeUser> = this.db.doc<StripeUser>(`users/${auth.uid}`)
      userDoc.get().toPromise().then((v) => {
        this.cusId = (v.data() as StripeUser).stripeCustomerId
      })
    })
  }

  ngOnInit() {
    let cart = JSON.parse(sessionStorage.getItem('cart')) as HotDrinkCart[]
    // this.cusId = window.localStorage.getItem('customer_id')
    cart.forEach((base) => {
      let stripeObj: ICheckoutLineItem = {
        name: `${base.name} - ${base.blend}`,
        amount: base.price,
        currency: 'gbp',
        // THIS IS VERY MUCH NOT THE WAY TO DO THIS
        quantity: 1
      }
      this.stripeCart.push(stripeObj)
    })
    sessionStorage.setItem('striped_cart', JSON.stringify(this.stripeCart))
  }

  async getCheckoutId() {
    console.log('fetching checkout id')
    const json = await fetch('/create_checkout_session', {
      method: 'POST',
      headers: {'Content-Type' :'application/json'},
      body: JSON.stringify({cart: this.stripeCart, customer: this.cusId})
    })
    this.pay(await json.json())
  }

  pay(json) {
    this.stripe.redirectToCheckout({
      sessionId: json.checkout_session_id
    })((res) => {
      console.log(res)
    })
  }
}
