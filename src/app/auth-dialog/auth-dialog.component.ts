import { Component, OnInit } from '@angular/core';
import { AuthProvider } from 'ngx-auth-firebaseui';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { MatDialogRef } from '@angular/material';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

// export interface StripeUser {
//   displayName: string,
//   email: string,
//   phoneNumber?: string,
//   photoUrl?: string,
//   providerId: string,
//   uid: string,
//   stripeCustomerId?: string
// }

export interface NonStripeUser {
  displayName: string,
  email: string,
  phoneNumber?: string,
  photoURL?: string,
  providerId: string,
  uid: string
}

export interface StripeUser extends NonStripeUser {
  stripeCustomerId: string
}

@Component({
  selector: 'app-auth-dialog',
  templateUrl: './auth-dialog.component.html',
  styleUrls: ['./auth-dialog.component.scss']
})
export class AuthDialogComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<AuthDialogComponent>, private afAuth: AngularFireAuth,
    private db: AngularFirestore) { }

  printUser(event) {
    this.dialogRef.close()
    let uid = this.afAuth.auth.currentUser.uid
    let userDoc: AngularFirestoreDocument<any> = this.db.doc<any>(`users/${uid}`)
    let user = userDoc.valueChanges() as Observable<any>
    let _user = userDoc.get().toPromise()
    _user.then((v) => {
      if ((v.data() as StripeUser).stripeCustomerId) {
        this.addCustomerToSession((v.data() as StripeUser).stripeCustomerId)
      } else {
        let s = v.data() as StripeUser
        this.createCustomer(s.email).then((e) => {
          let sUser: StripeUser = {
            displayName: s.displayName,
            email: s.email,
            phoneNumber: s.phoneNumber,
            photoURL: s.photoURL,
            providerId: s.providerId,
            uid: s.uid,
            stripeCustomerId: e
          }
          this.db.doc<StripeUser>(`users/${uid}`).update(sUser).then(() => console.log('Firestore updated'))
        })
      }
    })
    // user.subscribe((s) => {
    //   console.log(s as StripeUser)
    //   if (!(s as StripeUser).stripeCustomerId) {
    //     console.log('New user')
    //     this.createCustomer(s.email).then((e) => {
    //       let sUser: StripeUser = {
    //         displayName: s.displayName,
    //         email: s.email,
    //         phoneNumber: s.phoneNumber,
    //         photoURL: s.photoURL,
    //         providerId: s.providerId,
    //         uid: s.uid,
    //         stripeCustomerId: e
    //       }
    //       this.db.doc<StripeUser>(`users/${uid}`).update(sUser).then(() => console.log('Firestore update sent'))
    //       this.addCustomerToSession(e)
    //     })
    //   } else {
    //     this.addCustomerToSession((s as StripeUser).stripeCustomerId)
    //   }
    // })
    // window.location.reload()
  }

  addCustomerToSession(cusId: string) {
    localStorage.setItem('customer_id', cusId)
    // sessionStorage.setItem('uid', this.afAuth.auth.currentUser.uid)
  }

  async createCustomer(email: string): Promise<string> {
    const json = await fetch('/create_customer', {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({description: `Customer for ${email}`})
    })
    let j = await json.json()
    console.log(j)
    return j['customer']['id'] as string
  }

  printError(event) {
    console.error(event)
  }

  ngOnInit() {
  }

}
