import { Component, OnInit } from '@angular/core';
import { HotDrinkCart } from '../hot-drink/hot-drink.component';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  // TODO change to allow other drinks
  cart: HotDrinkCart[]
  price: number = 0

  constructor() { 
    this.cart = JSON.parse(sessionStorage.getItem('cart'))
    this.cart.forEach((cart) => {
      this.price += cart.price
    })
    sessionStorage.setItem('final_cart_price', `${this.price}`)
  }

  ngOnInit() {}

}
