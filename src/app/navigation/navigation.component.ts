import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AuthDialogComponent } from '../auth-dialog/auth-dialog.component';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  constructor(public dialog: MatDialog, public afAuth: AngularFireAuth) { }

  checkAccount() {
    if (this.afAuth.auth.currentUser == null) {
      let dialogRef = this.dialog.open(AuthDialogComponent, {
        width: '400'
      })
    } else {
      this.afAuth.auth.signOut().then(() => {window.location.reload()})
    }
  }

  ngOnInit() {}
}
