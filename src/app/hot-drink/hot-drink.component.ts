import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { HotDrinkCustomiserDialogComponent } from '../hot-drink-customiser-dialog/hot-drink-customiser-dialog.component';
import { MatDialog } from '@angular/material';

export interface HotDrink {
  available_blends?: string[],
  description: string,
  img: string,
  name: string,
  price: number
}

export interface HotDrinkCart {
  blend?: string,
  name: string,
  price: number
}

@Component({
  selector: 'app-hot-drink',
  templateUrl: './hot-drink.component.html',
  styleUrls: ['./hot-drink.component.scss']
})
export class HotDrinkComponent implements OnInit {
  hotDrinks: Observable<HotDrink[]>
  db: AngularFirestore
  cart: HotDrinkCart[]
  constructor(db: AngularFirestore, public dialog: MatDialog) { 
    console.log('Hot Drink')
    this.db = db
  }

  ngOnInit() {
    this.hotDrinks = this.db.collection('hot_drinks').valueChanges() as Observable<HotDrink[]>
    let currentCart = JSON.parse(sessionStorage.getItem('cart'))
    if (currentCart === null || currentCart === 'undefined') {
      this.cart = Array<HotDrinkCart>()
    } else {
      this.cart = currentCart
    }
  }

  ngOnDestroy() {
    sessionStorage.setItem('cart', JSON.stringify(this.cart))
  }

  drinkClicked(drink: HotDrink) {
    let dialogRef = this.dialog.open(HotDrinkCustomiserDialogComponent,
      {
        width: '400px',
        data: drink
      })
      dialogRef.afterClosed().subscribe(result => {
        let stripePrice = drink.price * 100
        if (!result) {
          console.log("shouldn't add")
        } else {
          this.cart.push({blend: result.blend, name: drink.name, price: stripePrice})
        }
      })
  }
}